The 'newchrome' shell script will invoke the Google Chrome web browser
in an OS X or desktop Linux environment in a temporary profile. The -h
option prints the following help text, with the $ variables replaced with
their values.


    usage: newchrome [ -hvi ] [ -z timezonestring ] [ name ]
           newchrome -Z YES

    Invoke Google Chrome in a new profile.

    The first time this script is invoked, a new profile will
    be created in the directory $NCDIR. Any
    customizations of the browser will then be saved in that profile.

    On subsequent invocations, the original profile will be copied into a
    temporary profile directory of the form $TMPPREFIX/newchrome-YYMMDD-HHMMSS and
    Chrome will be invoked in this temporary profile. The temporary profile
    is not automatically deleted, so these temporary directories will
    accumulate and may be manually deleted.

    If an argument 'name' is specified, generated directory names will
    include that as a component, allowing multiple distinct instances to be
    created.  This lets you have more than one original newchrome from which
    temporary clones will be created. Then the original profile directory
    becomes $NCDIR-$name and the temporary directory becomes
    $TMPPREFIX/newchrome-$name-YYMMDD-HHMMSS.

    A cloned browser as above may be reinvoked with the -r switch, which
    invokes Chrome in the most recently cloned profile.  So you may begin a
    cloned browser session, then exit it, then resume it with -r, as many
    times as needed.

    To re-customize the original profile use the -i option. Chrome will then
    be invoked in the original profile, and any changes made will then be
    saved and will become visible in future invocations without -i.

    -h      This help
    -v      Verbose/debugging output
    -i      Invoke original newchrome for additional initializations
    -r      Resume the most recent cloned instance
    -o opt  Pass on 'opt' to Chrome as an argument; opt may include
            any options for Chrome. Works best on Linux. E.g.:
              -o '--incognito --start-maximized'
    -z      Set the specified timezone for this invocation, into the
            TZ environment variable. E.g.:
              -z America/New_York
              -z Asia/Tokyo
              -z Europe/Berlin
              -z Etc/UTC
    -Z YES  Moves the original newchrome directory $NCDIR
            into $NCBACKUP and then invokes newchrome
            so a fresh NCDIR will be generated. This gives
            you a fresh newchrome while still allowing disaster recovery.
